// Get the element with id "hlaseni"
const hlaseniElement = document.getElementById("hlaseni");

// Check if the hlaseni element exists
if (hlaseniElement) {
    // Define a CSS animation for blinking
    const blinkAnimation = `
        @keyframes blink {
            0% { opacity: 1; }
            50% { opacity: 0; }
            100% { opacity: 1; }
        }
    `;

    // Create a <style> element and append the blink animation
    const styleElement = document.createElement("style");
    styleElement.innerHTML = blinkAnimation;
    document.head.appendChild(styleElement);

    // Apply the blink animation to the hlaseni element
    hlaseniElement.style.animation = "blink 1.5s infinite";
}