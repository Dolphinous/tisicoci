function startCountdown(element, targetDate, isEndTime) {
    let intervalId; // Declare intervalId here

    function updateCountdown() {
        const now = new Date();
        const remainingTime = targetDate - now;

        if (remainingTime <= 0) {
            if (isEndTime) { // If it's end time
                const overtime = -remainingTime;
                if (overtime > 3600000) { // If more than an hour has passed
                    clearInterval(intervalId);
                    element.style.display = 'none'; // Hide the element
                    return;
                }
                const hours = Math.floor(overtime / 1000 / 60 / 60);
                const minutes = Math.floor((overtime / 1000 / 60) % 60);
                const seconds = Math.floor((overtime / 1000) % 60);

                element.textContent = ` - (${hours}h ${minutes}m ${seconds}s po konci)`;
                element.style.color = 'red';
                return;
            }
            clearInterval(intervalId);
            element.style.display = 'none'; // Hide the element
            return;
        }

        const hours = Math.floor(remainingTime / 1000 / 60 / 60);
        const minutes = Math.floor((remainingTime / 1000 / 60) % 60);
        const seconds = Math.floor((remainingTime / 1000) % 60);

        if (isEndTime) { // If it's end time
            element.textContent = ` - (${hours}h ${minutes}m ${seconds}s do konce)`;
            element.style.color = 'yellow';
        } else {
            element.textContent = ` - (${hours}h ${minutes}m ${seconds}s do začátku)`;
        }
    }

    updateCountdown();
    intervalId = setInterval(updateCountdown, 1000); // Assign value to intervalId here

    return intervalId; // Return the interval ID in case you want to stop the countdown later
}

function applyCountdowns() {
    // Find all articles
    const articles = document.querySelectorAll('article');

    articles.forEach((article) => {
        // Extract the start and end times
        const startTimeElement = article.querySelector('.start-time');
        const endTimeElement = article.querySelector('.end-time');

        // If either start time or end time doesn't exist, skip this article
        if (!startTimeElement || !endTimeElement) {
            return;
        }

        const startTime = startTimeElement.textContent;
        const endTime = endTimeElement.textContent;

        // Parse the times
        const startHours = parseInt(startTime.split(':')[0]);
        const startMinutes = parseInt(startTime.split(':')[1]);
        const endHours = parseInt(endTime.split(':')[0]);
        const endMinutes = parseInt(endTime.split(':')[1]);

        // Create the target dates
        const startDate = new Date();
        startDate.setHours(startHours, startMinutes, 0, 0);
        const endDate = new Date();
        endDate.setHours(endHours, endMinutes, 0, 0);

        // Find the countdown elements
        const startCountdownElement = article.querySelector('.start-time-countdown');
        const endCountdownElement = article.querySelector('.end-time-countdown');

        // If either countdown element doesn't exist, skip this article
        if (!startCountdownElement || !endCountdownElement) {
            return;
        }

        // Hide the end countdown initially
        endCountdownElement.style.display = 'none';

        // Start the countdowns
        startCountdown(startCountdownElement, startDate, false);
        startCountdown(endCountdownElement, endDate, true);

        // Show the end countdown when the start time is reached
        if (startDate - new Date() > 0) {
            setTimeout(() => {
                endCountdownElement.style.display = '';
            }, startDate - new Date());
        } else {
            endCountdownElement.style.display = '';
        }
    });
}

// Apply the countdowns initially
applyCountdowns();

// Re-apply the countdowns whenever the content is updated
document.addEventListener('contentUpdated', applyCountdowns);