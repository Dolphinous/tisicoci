setTimeout(function () {
    // Dispatch the contentUpdated event
    let event = new Event('contentUpdated');
    document.dispatchEvent(event);
    location.reload();
}, 30000);