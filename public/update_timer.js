// Initialize the last update time
let lastUpdateTime = Date.now();

// Function to update the timer
function updateTimer() {
    let secondsSinceUpdate = Math.floor((Date.now() - lastUpdateTime) / 1000);
    let timerElement = document.getElementById('update-timer');
    if (secondsSinceUpdate <= 30) {
        timerElement.textContent = `Poslední aktualizace před: ${secondsSinceUpdate} s`;
        timerElement.style.color = '#f5f5f5';
    } else {
        timerElement.textContent = `Poslední aktualizace před: ${secondsSinceUpdate} s. Aktualizujte ručně!`;
        timerElement.style.color = 'red';
    }
}

// Update the timer every second
setInterval(updateTimer, 1000);

// Update the last update time whenever the content is updated
document.addEventListener('contentUpdated', () => {
    lastUpdateTime = Date.now();
});